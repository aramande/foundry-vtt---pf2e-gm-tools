const yargs = require('yargs');
const fs = require('fs');

const argv = yargs
    .option('buildno', {
        type: 'number',
        description: 'specifies the build number to be updated (CI_PIPELINE_IID)'
    })
    .option('branch', {
        'type': 'string',
        description: 'specifies the branch (CI_COMMIT_BRANCH)'
    })
    .option('gitlabpath', {
        type: 'string',
        description: 'The path on gitlab where this branch is stored (CI_PROJECT_PATH)'
    })
    .demandOption(['branch', 'buildno'])
    .argv;

const moduleRaw = fs.readFileSync('module.json');
let moduleJson = JSON.parse(moduleRaw);

moduleJson.version = `${moduleJson.version}.${argv.buildno}`;
moduleJson.url = `https://gitlab.com/${argv.gitlabpath}`;
moduleJson.manifest = `https://gitlab.com/${argv.gitlabpath}/-/jobs/artifacts/${argv.branch}/raw/system.json?job=build`;
moduleJson.download = `https://gitlab.com/${argv.gitlabpath}/-/jobs/artifacts/${argv.branch}/raw/pf2e.zip?job=build`;

fs.writeFileSync('system.json', JSON.stringify(moduleJson, null, 2));

console.log(moduleJson.manifest);
