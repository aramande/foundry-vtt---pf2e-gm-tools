# Aramande's PF2e GM Tools Module for FoundryVTT

🧰 Making lives easier for the experimental Game Masters in Pathfinder Second Edition games on FoundryVTT by adding calculators, variant rules and homebrew rules to the game.

## WIP and Feedback

> 🛠️ Please note that the functionality of this project is not complete and there is plenty of work remaining.

If you have any suggestions or feedback, please contact me on Discord @ **aramande#7662** or add an issue on gitlab.

## Licenses

**Project Licensing:**

- All HTML, CSS and Javascript in this project is licensed under the Apache License v2.

**Content Usage and Licensing:**

- Any Pathfinder Second Edition information used under the Paizo Inc. Community Use Policy (<https://paizo.com/community/communityuse>)
- Game system information and mechanics are licensed under the Open Game License (OPEN GAME LICENSE Version 1.0a).
- License information for the art used in this project is included in the ./packs/ folder alongside the JSON of where it is referenced.

**Virtual Table Top Platform Licenses:**

- Foundry VTT support is covered by the following license: [Limited License Agreement for module development 09/02/2020](https://foundryvtt.com/article/license/).

## Patch Notes

See [CHANGELOG.md](./CHANGELOG.md)

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md)
